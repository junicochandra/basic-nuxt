// mengecek apakah user sudah login / belum
export const state = () => ({
    isAuth: false
})

// memanipulasi state
export const mutations = {
    SET_IS_AUTH(state, payload) {
        state.isAuth = payload
    },
}

// memanipulasi isi dari state
export const actions = {
    // function untuk menjalankan otomatis ketika program nuxt di refresh
    nuxtServerInit({commit}, context) {
        // dari data ini di kirim ke mutation kita. cek lewat inspect element / console
        // console.log(context.app.$auth.$state.loggedIn)

        commit('SET_IS_AUTH', context.app.$auth.$state.loggedIn)
    }
}